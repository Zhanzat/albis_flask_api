from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

# init app
app = Flask(__name__)

# configuring our database uri
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+mysqlconnector://{user}@{server}/albis".format(user="root",
                                                                                              server="localhost")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)
# Init marshmallow
ma = Marshmallow(app)


class ProjectManager(db.Model):
    __tablename__ = "projectmanagers"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(50), unique=True)
    last_name = db.Column(db.String(50), unique=True)
    department = db.Column(db.String(50))

    def __init__(self, fname, lname, department):
        self.first_name = fname
        self.last_name = lname
        self.department = department


# Project Class/Model
class Project(db.Model):
    __tablename__ = "projects"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), unique=True)
    start = db.Column(db.DATE)
    end = db.Column(db.DATE)
    pm_id = db.Column(db.Integer, db.ForeignKey(ProjectManager.id))

    def __init__(self, name, start, end, pm_id):
        self.name = name
        self.start = start
        self.end = end
        self.pm_id = pm_id


# Project Schema
class ProjectManagerSchema(ma.Schema):
    class Meta:
        fields = ('first_name', 'last_name', 'department')


# Init schema
project_manager_schema = ProjectManagerSchema()
project_managers_schema = ProjectManagerSchema(many=True)


# Product Schema
class ProjectSchema(ma.Schema):
    class Meta:
        fields = ('name', 'start', 'end', 'pm_id')


# Init schema
project_schema = ProjectSchema()
projects_schema = ProjectSchema(many=True)


# Get All Projects
@app.route('/project', methods=['GET'])
def get_projects():
    all_projects = Project.query.all()
    result = projects_schema.dump(all_projects)
    return jsonify(result)


# Get All ProjectManagers
@app.route('/projectManager', methods=['GET'])
def get_project_managers():
    all_project_managers = ProjectManager.query.all()
    result = project_managers_schema.dump(all_project_managers)
    return jsonify(result)


# Get Single Project Managers
@app.route('/projectManager/<id>', methods=['GET'])
def get_project_manager(id):
    project_manager = ProjectManager.query.get(id)
    return project_manager_schema.jsonify(project_manager)


# Get Single Projects
@app.route('/project/<id>', methods=['GET'])
def get_project(id):
    project = Project.query.get(id)
    return project_schema.jsonify(project)


# Create a Project
@app.route('/project', methods=['POST'])
def add_project():
    name = request.json['name']
    end = request.json['end']
    start = request.json['start']
    pm_id = request.json['pm_id']

    new_project = Project(name, end, start, pm_id)

    db.session.add(new_project)
    db.session.commit()

    return project_schema.jsonify(new_project)


# Create a Project Manager
@app.route('/projectManager', methods=['POST'])
def add_project_manager():
    first_name = request.json['first_name']
    last_name = request.json['last_name']
    department = request.json['department']

    new_project_manager = ProjectManager(first_name, last_name, department)

    db.session.add(new_project_manager)
    db.session.commit()

    return project_manager_schema.jsonify(new_project_manager)


# Get Projects by Project Manager ID
@app.route('/dashboard/<pm_id>', methods=['GET'])
def get_projects_by_pm(pm_id):
    projects = Project.query.join(ProjectManager).filter(ProjectManager.id == pm_id)
    result = projects_schema.dump(projects)
    return jsonify(result)


# run server
if __name__ == '__main__':
    app.run()
